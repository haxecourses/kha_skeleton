# Kha Skeleton

## Description

Un framework qui peut servir de base pour des jeux programmés en Haxe utilisant le framework Kha.

​Aucune documentation n'est fournie pour l'instant.
Toutefois, pour faciliter la prise en main du framework, le code est clair et ​très largement commenté (en français uniquement pour le moment).

===

## Mouvements et actions du joueur

Déplacer le joueur: mouvements de la souris.
Déplacer le joueur: touches ou bien les flèches du clavier.
Action (tirer): clic gauche.
Mettre en pause: touche P.
Relancer la partie: touche R.

## Interactions

- **TODO**

## Copyrights

Ecrit en Haxe et en utilisant le Framework Kha.
Développé en utilisant principalement Visual studio code et Kode Studio.

---

(C) 2018 GameaMea <http://www.gameamea.com>

=================================================

## Description

A homemade framework that can be used as a base for games developed in Haxe and using the Kha Framework.

No documentation is provided at this time.
However, to facilitate the handling of the framework, the code is clear and widely commented (French only for the moment).

===

## Goals

- **TODO**

## Movements and actions of the player

Move the player: moves the mouse.
Move the player: ZQSD keys or the arrows on the keyboard.
Action (fire): left click
Pause: P key.
Restart the game: R key.

## Interactions

-**TODO**

## Copyrights

Written in Haxe and with the Kha Framework.
Developed mainly using Visual studio code and Kode Studio.

---

(C) 2018 GameaMea <http://www.gameamea.com>
