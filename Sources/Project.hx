import kha.Framebuffer;
import kha.Color;
import kha.input.Keyboard;
import kha.input.KeyCode;
import kha.input.Mouse;
import kha.math.FastMatrix3;
import kha.System;

class Project {
  public static inline var GRAVITY = 0.2;

  public var displayWidth:Int;
  public var displayHeight:Int;
  public var fire:Bool;

  var colBG:Color;

  @SuppressWarnings("checkstyle:VariableInitialisation")
  public var player = {
    x: 0.,
    y: 0.,
    w: 8.,
    h: 8.,
    vx: 1.,
    vy: 0.,
    color: Color.Blue
  };

  @SuppressWarnings("checkstyle:VariableInitialisation")
  var bomb = {
    x: 0.,
    y: 0.,
    w: 4.,
    h: 4.,
    vx: 0.,
    vy: 0.,
    color: Color.Red,
    alive: false
  };

  public function new(_screenWidth, _screenHeight) {
    if (Keyboard.get() != null) Keyboard.get().notify(onDown, onUp);
    if (Mouse.get() != null) Mouse.get().notify(onDownMouse, onUpMouse, null, null);
    displayWidth = _screenWidth;
    displayHeight = _screenHeight;
    colBG = Color.Black;
  }

  public function onDown(k:KeyCode) {
    trace('key $k down');
    fire = true;
  }

  public function onUp(k:KeyCode) {
    trace('key $k up');
    fire = false;
  }

  public function onDownMouse(button:Int, x:Int, y:Int) {
    trace('mouse button $button down');
    fire = true;
  }

  public function onUpMouse(button:Int, x:Int, y:Int) {
    trace('mouse button $button up');
    fire = false;
  }

  public function render(framebuffer:Framebuffer) {
    // paramètres de couleur
    if (fire) colBG = Color.White;
    else colBG = Color.Black;
    var transform = FastMatrix3.scale(System.windowWidth(0) / displayWidth, System.windowHeight(0) / displayHeight);
    // utilise les focntions gd2
    {
      var g = framebuffer.g2;
      g.begin();
      g.pushTransformation(transform);
      // Dessiner le fond
      {
        g.clear(colBG);
      }
      // Dessiner le joueur
      {
        g.color = player.color;
        g.fillRect(player.x, player.y, player.w, player.h);
      }
      // Dessiner la bombe
      if (bomb.alive) {
        g.color = bomb.color;
        g.fillRect(bomb.x, bomb.y, bomb.w, bomb.h);
      }
      g.popTransformation();
      g.end();
    }
  }

  public function update() {
    { // si nous avons appuyé sur le bouton, afficher la bombe
      if (fire && !bomb.alive) {
        bomb.alive = true;
        bomb.x = player.x;
        bomb.y = player.y;
        bomb.vx = player.vx;
        bomb.vy = 0.;
      }
    }
    // mouvement du joueur
    {
      player.x += player.vx;
      player.y += player.vy;
      // vérification des limites
      if (player.x > displayWidth) player.x = -player.w + 1;
      else if (player.x < -player.w) player.x = displayWidth + 1;
    }
    // movement de la bombe
    {
      if (bomb.alive) {
        bomb.vy += GRAVITY;
        bomb.x += bomb.vx;
        bomb.y += bomb.vy;
        // vérification des limites
        if (bomb.y > displayHeight) bomb.alive = false;
      }
    }
  }
}