import kha.Scheduler;
import kha.Framebuffer;
import kha.Window;
import kha.System;

class Main {
  // size of the window
  public static inline var WINDOW_WIDTH = 640;
  public static inline var WINDOWS_HEIGHT = 480;
  // size of the game. if different from the Window size , a scale will be automatically applied
  public static inline var GAME_WIDTH = 320;
  public static inline var GAME_HEIGHT = 240;
  // frames per second (refresh rate)
  public static inline var FPS = 60;

  public static function main() {
    System.start({title: "Kha_Skeleton", width: WINDOW_WIDTH, height: WINDOWS_HEIGHT}, initialized);
  }

  static function initialized(window:Window) {
    var game = new Project(GAME_WIDTH, GAME_HEIGHT);
    System.notifyOnFrames(function(fbs:Array<Framebuffer>) {
      game.render(fbs[0]);
    });
    Scheduler.addTimeTask(game.update, 0, 1 / FPS);
  }
}